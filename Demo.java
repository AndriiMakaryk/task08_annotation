package task08_Annotation;

import java.lang.reflect.Constructor;

public class Demo {

	public static void main(String[] args) {
		AnotationStudy anotation = new AnotationStudy();
		anotation.printValue();
		anotation.printStringValue();
		String name = anotation.getClass().getAnnotatedInterfaces().toString();
		System.out.println(name);
		Person person = new Person();
		Constructor<?>[] constructors = person.getClass().getConstructors();
		for (int i = 0; i < constructors.length; i++) {
			System.out.println(constructors[i].toString() + " ");
		}
	}
}
