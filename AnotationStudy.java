package task08_Annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.*;

public class AnotationStudy {

	@MyOwnAnnotation(value= 111)
	int value;
	

	public void printValue() {

		System.out.print(value);
	}
	public void printStringValue() {
		@MyOwnStringAnnotation("Andrii")
		String  name="";
		System.out.println(name);
	}

}
@interface MyOwnAnnotation{
	int value();
}

@interface MyOwnStringAnnotation {
	String value();
}